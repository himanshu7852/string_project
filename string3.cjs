
function getMonth(givenDate)
{
     
    let date=givenDate.split('/');
    const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let monthNumber=date[1];
    if(monthNumber<1||monthNumber>12)
    {
        return 'Not a Valid Date';
    }
    else{
        return monthNames[monthNumber-1];
    }
    
}

module.exports=getMonth;