function changeIPV4intoArray(ipAddress)
{
    let ipInArray=ipAddress.split('.');
    let count=ipInArray.length;
    let resultArray=[];
    if(count===4)
    {
        for(let index=0;index<4;index++)
        {
            if(isNaN(ipInArray[index]))
            {
                return [];
            }
            if(ipInArray[index]<0||ipInArray>255)
            {
                return [];
            }
            resultArray.push(Number(ipInArray[index]));
            

        }

        return resultArray;
    }
    else{
        return [];
    }

}
module.exports=changeIPV4intoArray;
