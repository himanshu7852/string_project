function stringConversion(numberString)
{
    if(numberString.charAt(0)!=='$' && numberString.charAt(1)!=='$')
    {
        return 0;
    }
    let dollerString=numberString.replace(/[^0-9.-]/g,"");

    for(let index=0;index<numberString.length;index++)
    {        
        let dotCount=0;
        if(numberString.charAt(index)==='.')
        {
            dotCount++;
        }  
        if(dotCount>1)
        {
            return Number(0);
        }      
    }
    return Number(dollerString);
}

module.exports=stringConversion;